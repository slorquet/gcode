/****************************************************************************
 * gcode/gcode_parser.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "gcode_parser.h"

/* supported commands */
struct code_s
{
  uint8_t        type;
  decimal_t value;
  uint8_t        group;
};

#define CODE_G 0
#define CODE_M 1

#define GCODE(val      ) CODE_G, DECIMAL_INIT_SINT(val)
#define GCODEF(val,frac) CODE_G, DECIMAL_INITIALIZER(0,val,frac*100000)
#define MCODE(val      ) CODE_M, DECIMAL_INIT_SINT(val)

struct code_s codes[] =
{
  /* Modal groups for G codes */
  {GCODE(0)    , GROUP_G_MOTION   }, /* Rapid positioning */
  {GCODE(1)    , GROUP_G_MOTION   }, /* Linear interpolation */
  {GCODE(2)    , GROUP_G_MOTION   }, /* Circular/helical interpolation (clockwise) */
  {GCODE(3)    , GROUP_G_MOTION   }, /* Circular/helical interpolation (counterclockwise) */
  {GCODEF(38,2), GROUP_G_MOTION   }, /* Straight probe */
  {GCODE(80)   , GROUP_G_MOTION   }, /* Cancel motion mode (including any canned cycle) */
  {GCODE(73)   , GROUP_G_MOTION   }, /* Canned cycle: Tormach high speed peck drilling */
  {GCODE(81)   , GROUP_G_MOTION   }, /* Canned cycle: drilling */
  {GCODE(82)   , GROUP_G_MOTION   }, /* Canned cycle: drilling with dwell */
  {GCODE(83)   , GROUP_G_MOTION   }, /* Canned cycle: peck drilling */
  {GCODE(84)   , GROUP_G_MOTION   }, /* Canned cycle: right hand tapping */
  {GCODE(85)   , GROUP_G_MOTION   }, /* Canned cycle: boring, no dwell, feed out */
  {GCODE(86)   , GROUP_G_MOTION   }, /* Canned cycle: boring, spindle stop, rapid out */
  {GCODE(87)   , GROUP_G_MOTION   }, /* Canned cycle: back boring */
  {GCODE(88)   , GROUP_G_MOTION   }, /* Canned cycle: boring, spindle stop, manual out */
  {GCODE(89)   , GROUP_G_MOTION   }, /* Canned cycle: boring, dwell, feed out */
  {GCODE(17)   , GROUP_G_PLANE    }, /* XY-plane selection */
  {GCODE(18)   , GROUP_G_PLANE    }, /* XZ-plane selection */
  {GCODE(19)   , GROUP_G_PLANE    }, /* YZ-plane selection */
  {GCODE(90)   , GROUP_G_DISTMODE }, /* Absolute distance mode */
  {GCODE(91)   , GROUP_G_DISTMODE }, /* Incremental distance mode */
  {GCODE(93)   , GROUP_G_FEEDMODE }, /* Inverse time feed rate mode */
  {GCODE(94)   , GROUP_G_FEEDMODE }, /* Units per minute feed rate mode */
  {GCODE(20)   , GROUP_G_UNITS    }, /* Inch system selection */
  {GCODE(21)   , GROUP_G_UNITS    }, /* Millimeter system selection */
  {GCODE(40)   , GROUP_G_RADCOMP  }, /* Cancel cutter radius compensation */
  {GCODE(41)   , GROUP_G_RADCOMP  }, /* Start cutter radius compensation left */
  {GCODE(42)   , GROUP_G_RADCOMP  }, /* Start cutter radius compensation right */
  {GCODE(43)   , GROUP_G_LENGTHOFF}, /* Tool length offset (plus) */
  {GCODE(49)   , GROUP_G_LENGTHOFF}, /* Cancel tool length offset */
  {GCODE(98)   , GROUP_G_CANNEDRET}, /* Initial level return in canned cycles */
  {GCODE(99)   , GROUP_G_CANNEDRET}, /* R-point level return in canned cycles */
  {GCODE(54)   , GROUP_G_COORDSYS }, /* Use preset work coordinate system 1 */
  {GCODE(55)   , GROUP_G_COORDSYS }, /* Use preset work coordinate system 2 */
  {GCODE(56)   , GROUP_G_COORDSYS }, /* Use preset work coordinate system 3 */
  {GCODE(57)   , GROUP_G_COORDSYS }, /* Use preset work coordinate system 4 */
  {GCODE(58)   , GROUP_G_COORDSYS }, /* Use preset work coordinate system 5 */
  {GCODE(59)   , GROUP_G_COORDSYS }, /* Use preset work coordinate system 6 */
  {GCODEF(59,1), GROUP_G_COORDSYS }, /* Use preset work coordinate system 7 */
  {GCODEF(59,2), GROUP_G_COORDSYS }, /* Use preset work coordinate system 8 */
  {GCODEF(59,3), GROUP_G_COORDSYS }, /* Use preset work coordinate system 9 */
  {GCODE(61)   , GROUP_G_PATHCTRL }, /* Set path control mode: exact path */
  {GCODEF(61,1), GROUP_G_PATHCTRL }, /* Set path control mode: exact stop */
  {GCODE(64)   , GROUP_G_PATHCTRL }, /* Set path control mode: continuous */
  /* Modal groups for M codes */
  {MCODE(0)    , GROUP_M_STOPPING }, /* Program stop */
  {MCODE(1)    , GROUP_M_STOPPING }, /* Optional program stop */
  {MCODE(2)    , GROUP_M_STOPPING }, /* Program end */
  {MCODE(30)   , GROUP_M_STOPPING }, /* Program end, pallet shuttle, and reset */
  {MCODE(60)   , GROUP_M_STOPPING }, /* Pallet shuttle and program stop */
  {MCODE(6)    , GROUP_M_TOOLCHG  }, /* Tool change */
  {MCODE(3)    , GROUP_M_SPINDLE  }, /* Turn spindle clockwise */
  {MCODE(4)    , GROUP_M_SPINDLE  }, /* Turn spindle counterclockwise */
  {MCODE(5)    , GROUP_M_SPINDLE  }, /* Stop spindle turning */
  {MCODE(7)    , GROUP_M_COOLANT  }, /* Mist coolant on */
  {MCODE(8)    , GROUP_M_COOLANT  }, /* Flood coolant on */
  {MCODE(9)    , GROUP_M_COOLANT  }, /* Mist and flood coolant off */
  {MCODE(48)   , GROUP_M_FEEDOVRD }, /* Enable speed and feed overrides */
  {MCODE(49)   , GROUP_M_FEEDOVRD }, /* Disable speed and feed overrides */
  /* Non-modal G codes */
  {GCODE(4)    , GROUP_G_NONMODAL }, /* Dwell */
  {GCODE(10)   , GROUP_G_NONMODAL }, /* Coordinate system origin setting */
  {GCODE(28)   , GROUP_G_NONMODAL }, /* Return to home */
  {GCODE(30)   , GROUP_G_NONMODAL }, /* Return to secondary home */
  {GCODE(53)   , GROUP_G_NONMODAL }, /* Motion in machine coordinate system */
  {GCODE(92)   , GROUP_G_NONMODAL }, /* Offset coordinate systems and set parameters */
  {GCODEF(92,1), GROUP_G_NONMODAL }, /* Cancel offset coordinate systems and set parameters to zero */
  {GCODEF(92,2), GROUP_G_NONMODAL }, /* Cancel offset coordinate systems but do not reset parameters */
  {GCODEF(92,3), GROUP_G_NONMODAL }, /* Apply parameters to offset coordinate systems */
};
#define NUM_CODES (sizeof(codes)/sizeof(codes[0]))

static int find_group(int type, FAR decimal_t *dec)
{
  int i;

  for(i=0;i<NUM_CODES;i++)
    {
      if(codes[i].type == type)
        {
          if(codes[i].value.integer == dec->integer &&
             codes[i].value.frac    == dec->frac)
            {
              return codes[i].group;
            }
        }
    }
  return -1;
}

int gcode_parse(FAR struct gcode_command_s *command, FAR char *ptr)
{
  char letter;
  FAR char *next;
  decimal_t dec;
  int numlen;
  int id = 0;
  int mcount = 0;

  command->ggroups = 0;
  command->mgroups = 0;
  command->aflags  = 0;
  command->pflags  = 0;

  //printf("len=%d\n",strlen(ptr));

  //Parse groups until end of line
  while(*ptr)
    {
      dec.neg  = 0;
      dec.frac = 0;

      //printf("[%s]\n",ptr);

      /* skip whitespace */
      while(*ptr!=0 && (*ptr==' ' || *ptr=='\t'))
        {
          ptr++;
          //printf("skip wsp\n");
        }

      /* skip comments */
      if(*ptr=='(')
        {
          while(*ptr!=0 && *ptr!=')')
            {
              ptr++;
              //printf("skip cmt\n");
            }
            ptr++;
        }

      /* skip whitespace */
      while(*ptr!=0 && (*ptr==' ' || *ptr=='\t'))
        {
          ptr++;
          //printf("skip wsp\n");
        }

      if(*ptr==0)
        {
          //printf("empty\n");
          break; //empty statement
        }

      /* read letter */
      letter = *ptr;
      if(letter >='a' && letter <= 'z')
        {
          //printf("lower to upper\n");
          letter -= 0x20;
        }
      if(letter <'A' || letter > 'Z')
        {
          //printf("not a letter\n");
          return ERROR_SYNTAX;
        }

      ptr++;

      /* skip whitespace */
      while(*ptr!=0 && (*ptr==' ' || *ptr=='\t'))
        {
          ptr++;
          //printf("skip wsp\n");
        }

      if(*ptr==0)
        {
          //printf("no number after letter\n");
          return ERROR_SYNTAX;
        }

      /* read and number sign */
      if(*ptr=='-')
        {
          dec.neg = 1;
          ptr++;
          //printf("minus sign\n");
        }
      else if(*ptr=='+')
        {
          ptr++;
          //printf("plus sign\n");
        }

      if(*ptr != '.' &&(*ptr<'0' || *ptr>'9'))
        {
          printf("garbage before number\n");
          return ERROR_SYNTAX;
        }

      if(*ptr==0)
        {
          printf("lonely sign\n");
          return ERROR_SYNTAX;
        }
      
      /* read number integer */
      set_errno(0);
      dec.integer = strtoul(ptr, &next, 10);
      if(errno!=0)
        {
          printf("bad number\n");
          return ERROR_SYNTAX; //bad number;
        }

      ptr = next;

      /* check for decimal dot */
      if(*ptr=='.')
        {
          FAR char *num;

          //printf("decimal dot after int\n");
          ptr++;

          if(*ptr !=0 && *ptr!=' ' && (*ptr<'0' || *ptr>'9'))
            {
              printf("garbage before second number\n");
              return ERROR_SYNTAX;
            }

          numlen=0;
          num = ptr;
          while(*num && *num>='0' && *num<='9')
            {
              numlen++;
              num++;
            }

          /* if number must be truncated, truncate if not end of string*/
          if(numlen>DECIMAL_DIGITS && ptr[DECIMAL_DIGITS]!=0)
            {
            ptr[DECIMAL_DIGITS]='/'; //non-decimal digit
//            printf("number too long, truncated: %s\n", ptr);
            }

          set_errno(0);
          dec.frac = strtoul(ptr, &next, 10);
          if(errno!=0)
            {
              printf("bad second number\n");
              return ERROR_SYNTAX; //bad number;
            }

          ptr += numlen; //skip ALL number even if truncated

          /*if number has fewer than max digit, pad it right*/
          while(numlen<DECIMAL_DIGITS)
            {
            dec.frac *= 10;
            numlen++;
            }
        }

      //interpret
      //format size http://stackoverflow.com/questions/5932214/printf-string-variable-length-item

      if(letter=='G')
        {
          int group = find_group(CODE_G, &dec);
          if(group>=0 && group<GROUP_G_COUNT)
            {
              /* check a command of the same group was not parsed before */
              if(command->ggroups & (1<<group))
                {
                  return ERROR_MODALGROUPS;
                }
              memcpy(&command->gvals[group], &dec, sizeof(decimal_t));
              command->ggroups |= (1<<group);
            }
        }
      else if(letter=='M')
        {
          int group;
          mcount += 1;
          if(mcount==4)
            {
            return ERROR_MODALGROUPS; // Max 4 M codes allowed on a line
            }
          group = find_group(CODE_M, &dec);
          if(group>=0 && group<GROUP_M_COUNT)
            {
              /* check a command of the same group was not parsed before */
              if(command->mgroups & (1<<group))
                {
                  return ERROR_MODALGROUPS;
                }
              memcpy(&command->mvals[group], &dec, sizeof(decimal_t));
              command->mgroups |= (1<<group);
            }
        }
      else
        {
          /* if a parameter appears more than once the last value is kept */
          switch(letter)
            {
              case 'A':
                if(command->aflags & FLAG_A) return ERROR_WORD_DUPLICATE;
                command->aflags |= FLAG_A;
                memcpy(&command->axes   [AXIS_A  ], &dec, sizeof(decimal_t));
                break;

              case 'B':
                if(command->aflags & FLAG_B) return ERROR_WORD_DUPLICATE;
                command->aflags |= FLAG_B;
                memcpy(&command->axes   [AXIS_B  ], &dec, sizeof(decimal_t));
                break;

              case 'C':
                if(command->aflags & FLAG_C) return ERROR_WORD_DUPLICATE;
                command->aflags |= FLAG_C;
                memcpy(&command->axes   [AXIS_C  ], &dec, sizeof(decimal_t));
                break;

              case 'D':
                if(command->pflags & FLAG_D) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_D;
                memcpy(&command->params [PARAM_D ], &dec, sizeof(decimal_t));
                break;

              case 'F':
                if(command->pflags & FLAG_F) return ERROR_WORD_DUPLICATE;
		command->pflags |= FLAG_F;
                memcpy(&command->params [PARAM_F ], &dec, sizeof(decimal_t));
                break;

              case 'H':
                if(command->pflags & FLAG_H) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_H;
                memcpy(&command->params [PARAM_H ], &dec, sizeof(decimal_t));
                break;

              case 'I':
                if(command->pflags & FLAG_I) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_I;
                memcpy(&command->params [PARAM_I ], &dec, sizeof(decimal_t));
                break;

              case 'J':
                if(command->pflags & FLAG_J) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_J;
                memcpy(&command->params [PARAM_J ], &dec, sizeof(decimal_t));
                break;

              case 'K':
                if(command->pflags & FLAG_K) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_K;
                memcpy(&command->params [PARAM_K ], &dec, sizeof(decimal_t));
                break;

              case 'L':
                if(command->pflags & FLAG_L) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_L;
                memcpy(&command->params [PARAM_L ], &dec, sizeof(decimal_t));
                break;

              case 'P':
                if(command->pflags & FLAG_P) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_P;
                memcpy(&command->params [PARAM_P ], &dec, sizeof(decimal_t));
                break;

              case 'Q':
                if(command->pflags & FLAG_Q) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_Q;
                memcpy(&command->params [PARAM_Q ], &dec, sizeof(decimal_t));
                break;

              case 'R':
                if(command->pflags & FLAG_R) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_R;
                memcpy(&command->params [PARAM_R ], &dec, sizeof(decimal_t));
                break;

              case 'S':
                if(command->pflags & FLAG_S) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_S;
                memcpy(&command->params [PARAM_S ], &dec, sizeof(decimal_t));
                break;

              case 'T':
                if(command->pflags & FLAG_T) return ERROR_WORD_DUPLICATE;
                command->pflags |= FLAG_T;
                memcpy(&command->params [PARAM_T ], &dec, sizeof(decimal_t));
                break;

              case 'X':
                if(command->aflags & FLAG_X) return ERROR_WORD_DUPLICATE;
                command->aflags |= FLAG_X;
                memcpy(&command->axes   [AXIS_X  ], &dec, sizeof(decimal_t));
                break;

              case 'Y':
                if(command->aflags & FLAG_Y) return ERROR_WORD_DUPLICATE;
                command->aflags |= FLAG_Y;
                memcpy(&command->axes   [AXIS_Y  ], &dec, sizeof(decimal_t));
                break;

              case 'Z':
                if(command->aflags & FLAG_Z) return ERROR_WORD_DUPLICATE;
                command->aflags |= FLAG_Z;
                memcpy(&command->axes   [AXIS_Z  ], &dec, sizeof(decimal_t));
                break;

              case 'N':
                if(id!=0)
                  {
                    return ERROR_LINE_NUMBER;
                  }
                if(dec.frac != 0)
                  {
                    return ERROR_INTEGER_REQUIRED;
                  }
                if(dec.neg)
                  {
                    return ERROR_SYNTAX;
                  }
                command->pflags |= FLAG_N; 
                memcpy(&command->params [PARAM_N ], &dec, sizeof(decimal_t)); 
                break;

              default: return ERROR_UNKNOWN_LETTER;
            }
        }

      id++; //next word

    } //while

  return 0;
}

void gcode_command_show(FAR struct gcode_command_s *command)
{
  int i;
  int flag = 0;

  printf("Ggroups %04X Mgroups %04X Aflags %04X Pflags %04X\n", command->ggroups, command->mgroups, command->aflags, command->pflags);
  if(command->pflags & FLAG_N) printf("Line number %u\n", command->params[PARAM_N].integer);

  /* Display modes */
  flag = 0;
  for(i=0; i<GROUP_G_COUNT; i++)
    {
      if(command->ggroups & (1<<i)) { flag=1; printf(" G %u.%.*u", command->gvals[i].integer, DECIMAL_DIGITS, command->gvals[i].frac); }
    }
  if(flag) printf("\n");
  flag = 0;
  for(i=0; i<GROUP_M_COUNT; i++)
    {
      if(command->mgroups & (1<<i)) { flag=1; printf(" M %u.%.*u", command->mvals[i].integer, DECIMAL_DIGITS, command->mvals[i].frac); }
    }
  if(flag) printf("\n");

  /* Display axes */
  flag=0;
  if(command->aflags & FLAG_X) { flag=1; printf(" X %s%u.%.*u", command->axes[AXIS_X].neg?"-":"", command->axes[AXIS_X].integer, DECIMAL_DIGITS, command->axes[AXIS_X].frac); }
  if(command->aflags & FLAG_Y) { flag=1; printf(" Y %s%u.%.*u", command->axes[AXIS_Y].neg?"-":"", command->axes[AXIS_Y].integer, DECIMAL_DIGITS, command->axes[AXIS_Y].frac); }
  if(command->aflags & FLAG_Z) { flag=1; printf(" Z %s%u.%.*u", command->axes[AXIS_Z].neg?"-":"", command->axes[AXIS_Z].integer, DECIMAL_DIGITS, command->axes[AXIS_Z].frac); }
  if(flag) printf("\n");

  /* Display parameters */
  flag=0;
  if(command->aflags & FLAG_A) { flag=1; printf(" A %s%u.%.*u", command->axes[AXIS_A].neg?"-":"", command->axes[AXIS_A].integer, DECIMAL_DIGITS, command->axes[AXIS_A].frac); }
  if(command->aflags & FLAG_B) { flag=1; printf(" B %s%u.%.*u", command->axes[AXIS_B].neg?"-":"", command->axes[AXIS_B].integer, DECIMAL_DIGITS, command->axes[AXIS_B].frac); }
  if(command->aflags & FLAG_C) { flag=1; printf(" C %s%u.%.*u", command->axes[AXIS_C].neg?"-":"", command->axes[AXIS_C].integer, DECIMAL_DIGITS, command->axes[AXIS_C].frac); }
  if(flag) printf("\n");

  flag=0;
  if(command->pflags & FLAG_I) { flag=1; printf(" I %s%u.%.*u", command->params[PARAM_I].neg?"-":"", command->params[PARAM_I].integer, DECIMAL_DIGITS, command->params[PARAM_I].frac); }
  if(command->pflags & FLAG_J) { flag=1; printf(" J %s%u.%.*u", command->params[PARAM_J].neg?"-":"", command->params[PARAM_J].integer, DECIMAL_DIGITS, command->params[PARAM_J].frac); }
  if(command->pflags & FLAG_K) { flag=1; printf(" K %s%u.%.*u", command->params[PARAM_K].neg?"-":"", command->params[PARAM_K].integer, DECIMAL_DIGITS, command->params[PARAM_K].frac); }
  if(command->pflags & FLAG_R) { flag=1; printf(" R %s%u.%.*u", command->params[PARAM_R].neg?"-":"", command->params[PARAM_R].integer, DECIMAL_DIGITS, command->params[PARAM_R].frac); }
  if(flag) printf("\n");

  flag=0;
  if(command->pflags & FLAG_T) { flag=1; printf(" T %s%u.%.*u", command->params[PARAM_T].neg?"-":"", command->params[PARAM_T].integer, DECIMAL_DIGITS, command->params[PARAM_T].frac); }
  if(command->pflags & FLAG_D) { flag=1; printf(" D %s%u.%.*u", command->params[PARAM_D].neg?"-":"", command->params[PARAM_D].integer, DECIMAL_DIGITS, command->params[PARAM_D].frac); }
  if(command->pflags & FLAG_H) { flag=1; printf(" H %s%u.%.*u", command->params[PARAM_H].neg?"-":"", command->params[PARAM_H].integer, DECIMAL_DIGITS, command->params[PARAM_H].frac); }
  if(flag) printf("\n");

  flag=0;
  if(command->pflags & FLAG_S) { flag=1; printf(" S %s%u.%.*u", command->params[PARAM_S].neg?"-":"", command->params[PARAM_S].integer, DECIMAL_DIGITS, command->params[PARAM_S].frac); }
  if(command->pflags & FLAG_F) { flag=1; printf(" F %s%u.%.*u", command->params[PARAM_F].neg?"-":"", command->params[PARAM_F].integer, DECIMAL_DIGITS, command->params[PARAM_F].frac); }
  if(command->pflags & FLAG_Q) { flag=1; printf(" Q %s%u.%.*u", command->params[PARAM_Q].neg?"-":"", command->params[PARAM_Q].integer, DECIMAL_DIGITS, command->params[PARAM_Q].frac); }
  if(flag) printf("\n");

  flag=0;
  if(command->pflags & FLAG_L) { flag=1; printf(" L %s%u.%.*u", command->params[PARAM_L].neg?"-":"", command->params[PARAM_L].integer, DECIMAL_DIGITS, command->params[PARAM_L].frac); }
  if(command->pflags & FLAG_P) { flag=1; printf(" P %s%u.%.*u", command->params[PARAM_P].neg?"-":"", command->params[PARAM_P].integer, DECIMAL_DIGITS, command->params[PARAM_P].frac); }
  if(flag) printf("\n");
}

