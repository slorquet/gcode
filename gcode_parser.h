#ifndef __GCODE_PARSER_H__
#define __GCODE_PARSER_H__

#include <stdint.h>

#include "decimal.h"

enum axes
{
  AXIS_X,
  AXIS_Y,
  AXIS_Z,
  AXIS_A,
  AXIS_B,
  AXIS_C,
  AXIS_COUNT
};

enum params
{
  PARAM_D, //-tool radius compensation
  PARAM_F, //-feedrate
  PARAM_H, //-tool length compensation
  PARAM_I, //-
  PARAM_J, //-
  PARAM_K, //-
  PARAM_L, //repetitions for canned cycles
  PARAM_N, //-line number
  PARAM_P, //dwell time
  PARAM_Q, //-feed increment
  PARAM_R, //-arc radius
  PARAM_S, //-spindle speed
  PARAM_T, //-tool selection
  PARAM_COUNT
};

enum flags_params
{
  FLAG_D  = (1<<PARAM_D),
  FLAG_F  = (1<<PARAM_F),
  FLAG_H  = (1<<PARAM_H),
  FLAG_I  = (1<<PARAM_I),
  FLAG_J  = (1<<PARAM_J),
  FLAG_K  = (1<<PARAM_K),
  FLAG_L  = (1<<PARAM_L),
  FLAG_N  = (1<<PARAM_N),
  FLAG_P  = (1<<PARAM_P),
  FLAG_Q  = (1<<PARAM_Q),
  FLAG_R  = (1<<PARAM_R),
  FLAG_S  = (1<<PARAM_S),
  FLAG_T  = (1<<PARAM_T),
  FLAG_IJ = FLAG_I | FLAG_J,
  FLAG_JK = FLAG_J | FLAG_K,
  FLAG_IK = FLAG_K | FLAG_I
};

enum flags_axes
{
  FLAG_X  = (1<<AXIS_X),
  FLAG_Y  = (1<<AXIS_Y),
  FLAG_Z  = (1<<AXIS_Z),
  FLAG_A  = (1<<AXIS_A),
  FLAG_B  = (1<<AXIS_B),
  FLAG_C  = (1<<AXIS_C),
  FLAG_XY = FLAG_X | FLAG_Y,
  FLAG_YZ = FLAG_Y | FLAG_Z,
  FLAG_XZ = FLAG_Z | FLAG_X

};

/* modal groups */
enum modal_group_g
{
  GROUP_G_NONMODAL, /*  0 Non-modal commands*/
  GROUP_G_MOTION,   /*  1 Motion */
  GROUP_G_PLANE,    /*  2 Plane Selection */
  GROUP_G_DISTMODE, /*  3 Distance Mode */
  GROUP_G_FEEDMODE, /*  5 Feed rate mode */
  GROUP_G_UNITS,    /*  6 Units */
  GROUP_G_RADCOMP,  /*  7 Cutter radius Compensation */
  GROUP_G_LENGTHOFF,/*  8 Tool Length Offset */
  GROUP_G_CANNEDRET,/* 10 Return Mode in Canned Cycles */
  GROUP_G_COORDSYS, /* 12 Coordinate System Selection */
  GROUP_G_PATHCTRL, /* 13 Path Control Mode */
  GROUP_G_COUNT
};

enum modal_group_m
{
  GROUP_M_STOPPING, /*  4 Stopping */
  GROUP_M_TOOLCHG,  /*  6 Tool Change */
  GROUP_M_SPINDLE,  /*  7 Spindle */
  GROUP_M_COOLANT,  /*  8 Coolant */
  GROUP_M_FEEDOVRD, /*  9 Feed/Speed switches overrides */
  GROUP_M_COUNT
};

//parsing errors
enum gcode_parse_errors
{
  GCODE_OK,
  ERROR_SYNTAX,
  ERROR_UNKNOWN_LETTER,
  ERROR_MODALGROUPS,  //Modal group conflict
  ERROR_LINE_NUMBER, //N word must be the first one,
  ERROR_WORD_DUPLICATE,
  ERROR_INTEGER_REQUIRED,
  //Execution errors
  ERROR_AXIS_REQUIRED,
};

struct gcode_command_s
{
  decimal_t gvals[GROUP_G_COUNT];
  decimal_t mvals[GROUP_M_COUNT];
  decimal_t axes[AXIS_COUNT];    // X Y Z A B C
  decimal_t params[PARAM_COUNT]; //D F H I J K L N P Q R S T
  uint16_t aflags;  //One bit for each axis
  uint16_t pflags;  //One bit for each param
  uint16_t ggroups;  //One bit for each G modal group
  uint16_t mgroups;  //One bit for each M modal group
};

int gcode_parse(FAR struct gcode_command_s *command, FAR char *ptr);
void gcode_command_show(FAR struct gcode_command_s *command);

#endif // __GCODE_PARSER_H__
