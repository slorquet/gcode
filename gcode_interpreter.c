/****************************************************************************
 * gcode/gcode_interpreter.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "gcode_parser.h"
#include "gcode_interpreter.h"

int gcode_interp_init(FAR struct gcode_state_s *parser,
                      FAR struct gcode_output_s *output, FAR void *context)
{
  int ret;
  decimal_t zero = DECIMAL_INIT_SINT(0);

  parser->out = output;
  parser->ctx = context;

  memset(&parser->gmodes , 0, sizeof(parser->gmodes)); 
  memset(&parser->mmodes , 0, sizeof(parser->mmodes)); 
  memset(&parser->axes   , 0, sizeof(parser->axes)); 
  memset(&parser->offsets, 0, sizeof(parser->axes)); 

  ret = parser->out->init(parser->ctx);
  if(ret != 0) return ret;

  /* set default values */
  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_MOTION   ], 80); //Motion: None
  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_DISTMODE ], 90); //Distance mode: Absolute
  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_FEEDMODE ], 93); //Feed rate mode: units/min
  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_CANNEDRET], 98); //Return mode in canned cycles
  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_COORDSYS ], 54); //Coordinate system selection (1)

  DECIMAL_SET_INT(&parser->mmodes[GROUP_M_STOPPING ],  0);  //Stopping: none
  DECIMAL_SET_INT(&parser->mmodes[GROUP_M_TOOLCHG  ],  0);  //Tool change

  parser->t_value = 0;
  parser->feed = zero;


  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_PLANE    ], 17); //Plane selection: XY
  ret = parser->out->select_plane(parser->ctx, PLANE_XY);
  if(ret != 0) return ret;

  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_UNITS    ], 20); //Units: mm
  ret = parser->out->use_length_units(parser->ctx, UNITS_MM);
  if(ret != 0) return ret;

  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_RADCOMP  ], 40); //Cutter radius compensation: None
  ret = parser->out->stop_cutter_radius_compensation(parser->ctx);
  if(ret != 0) return ret;

  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_LENGTHOFF], 49); //Tool length offset (canceled)
  ret = parser->out->use_tool_length_offset(parser->ctx, zero);
  if(ret != 0) return ret;

  DECIMAL_SET_INT(&parser->gmodes[GROUP_G_PATHCTRL ], 61); //Path control mode
  ret = parser->out->set_motion_control_mode(parser->ctx, MODE_EXACT_STOP);
  if(ret != 0) return ret;

  parser->speed = zero;
  DECIMAL_SET_INT(&parser->mmodes[GROUP_M_SPINDLE  ],  5);  //Spindle Turning: None
  ret = parser->out->stop_spindle_turning(parser->ctx);
  if(ret != 0) return ret;

  DECIMAL_SET_INT(&parser->mmodes[GROUP_M_COOLANT  ], 9);  //Coolant: None
  ret = parser->out->flood_off(parser->ctx);
  if(ret != 0) return ret;
  ret = parser->out->mist_off(parser->ctx);
  if(ret != 0) return ret;

  DECIMAL_SET_INT(&parser->mmodes[GROUP_M_FEEDOVRD ], 48); //Feed override switches: Enabled
  ret = parser->out->enable_feed_override(parser->ctx);
  if(ret != 0) return ret;
  ret = parser->out->enable_speed_override(parser->ctx);
  if(ret != 0) return ret;

}

int gcode_interp_show(FAR struct gcode_state_s *parser)
{
  int i;
  decimal_t loc[6];
  printf("Interpreter State:\n");
  for(i=1;i<GROUP_G_COUNT;i++) //Group index zero is not persistent modal group
    {
      printf("G%s ", decimal_str(&parser->gmodes[i]));
    }
  for(i=0;i<GROUP_M_COUNT;i++)
    {
      printf("M%s ", decimal_str(&parser->gmodes[i]));
    }
  printf("\nS %s " , decimal_str(&parser->speed));
  printf("F %s "   , decimal_str(&parser->feed));
  printf("T %d\n"  , parser->t_value);
  printf(" X %s "  , decimal_str(&parser->axes[AXIS_X]));
  printf(" Y %s "  , decimal_str(&parser->axes[AXIS_Y]));
  printf(" Z %s "  , decimal_str(&parser->axes[AXIS_Z]));
  printf(" A %s "  , decimal_str(&parser->axes[AXIS_A]));
  printf(" B %s "  , decimal_str(&parser->axes[AXIS_B]));
  printf(" C %s\n" , decimal_str(&parser->axes[AXIS_C]));

  printf("OX %s "  , decimal_str(&parser->offsets[AXIS_X]));
  printf("OY %s "  , decimal_str(&parser->offsets[AXIS_Y]));
  printf("OZ %s "  , decimal_str(&parser->offsets[AXIS_Z]));
  printf("OA %s "  , decimal_str(&parser->offsets[AXIS_A]));
  printf("OB %s "  , decimal_str(&parser->offsets[AXIS_B]));
  printf("OC %s\n" , decimal_str(&parser->offsets[AXIS_C]));

  for(i=0;i<6;i++) decimal_add(&loc[i], &parser->axes[i], &parser->offsets[i]);

  printf("LX %s "  , decimal_str(&loc[AXIS_X]));
  printf("LY %s "  , decimal_str(&loc[AXIS_Y]));
  printf("LZ %s "  , decimal_str(&loc[AXIS_Z]));
  printf("LA %s "  , decimal_str(&loc[AXIS_A]));
  printf("LB %s "  , decimal_str(&loc[AXIS_B]));
  printf("LC %s\n" , decimal_str(&loc[AXIS_C]));

  return 0;
}

int gcode_interpret(FAR struct gcode_state_s *parser,
                    FAR struct gcode_command_s *command)
{
  int      i;
  int      ret;
  int      useful_axis[3];
  int      useful_offset[2];
  uint16_t axis_mask, offset_mask;
  bool     arc_radius_mode;

  /* 1. comment (includes message). */

  /* 2. set feed rate mode (G93, G94 — inverse time or per minute). */
  if(command->ggroups & (1<<GROUP_G_FEEDMODE))
    {
      printf("Set feed rate mode: G%s\n", decimal_str(&command->gvals[GROUP_G_FEEDMODE]));

      parser->gmodes[GROUP_G_FEEDMODE] = command->gvals[GROUP_G_FEEDMODE];
      command->ggroups &= ~(1<<GROUP_G_FEEDMODE);
    }

  /* 3. set feed rate (F). */
  if(command->pflags & FLAG_F)
    {
      parser->feed = command->params[PARAM_F];
      ret = parser->out->set_feed_rate(parser->ctx, command->params[PARAM_F]);
    }

  /* 4. set spindle speed (S). */
  if(command->pflags & FLAG_S)
    {
      parser->speed = command->params[PARAM_S];
      ret = parser->out->set_spindle_speed(parser->ctx, command->params[PARAM_S]);
    }

  /* 5. select tool (T). */
  if(command->pflags & FLAG_T)
    {
      parser->t_value = DECIMAL_INT(&command->params[PARAM_T]);
      ret = parser->out->select_tool(parser->ctx, parser->t_value);
    }

  /* 6. change tool (M6). */
  if(command->mgroups & (1<<GROUP_M_TOOLCHG))
    {
      ret = parser->out->change_tool(parser->ctx, parser->t_value);
      command->mgroups &= ~(1<<GROUP_M_TOOLCHG);
    }

  /* 7. spindle on or off (M3, M4, M5). */
  if(command->mgroups & (1<<GROUP_M_SPINDLE))
    {
      int cmd = DECIMAL_INT(&command->mvals[GROUP_M_SPINDLE]);

      if(cmd == 3)
        {
          ret = parser->out->start_spindle_clockwise(parser->ctx);
        }
      else if(cmd == 4)
        {
          ret = parser->out->start_spindle_counterclockwise(parser->ctx);
        }
      else if(cmd == 5)
        {
          ret = parser->out->stop_spindle_turning(parser->ctx);
        }

      parser->mmodes[GROUP_M_SPINDLE] = command->mvals[GROUP_M_SPINDLE];
      command->mgroups &= ~(1<<GROUP_M_SPINDLE);
    }

  /* 8. coolant on or off (M7, M8, M9). */
  if(command->mgroups & (1<<GROUP_M_COOLANT))
    {
      int cmd = DECIMAL_INT(&command->mvals[GROUP_M_COOLANT]);

      if(cmd == 7)
        {
          ret = parser->out->mist_on(parser->ctx);
        }
      else if(cmd == 8)
        {
          ret = parser->out->flood_on(parser->ctx);
        }
      else if(cmd == 9)
        {
          ret = parser->out->mist_off (parser->ctx);
          ret = parser->out->flood_off(parser->ctx);
        }
      
      parser->mmodes[GROUP_M_COOLANT] = command->mvals[GROUP_M_COOLANT];
      command->mgroups &= ~(1<<GROUP_M_COOLANT);
    }

  /* 9. enable or disable overrides (M48, M49). */
  if(command->mgroups & (1<<GROUP_M_FEEDOVRD))
    {
      int cmd = DECIMAL_INT(&command->mvals[GROUP_M_FEEDOVRD]);

      if(cmd == 48)
        {
          ret = parser->out->enable_feed_override(parser->ctx);
          ret = parser->out->enable_speed_override(parser->ctx);
        }
      else if(cmd == 49)
        {
          ret = parser->out->disable_feed_override(parser->ctx);
          ret = parser->out->disable_speed_override(parser->ctx);
        }

      parser->mmodes[GROUP_M_FEEDOVRD] = command->mvals[GROUP_M_FEEDOVRD];
      command->mgroups &= ~(1<<GROUP_M_FEEDOVRD);
    }

  /* 10. dwell (G4). */
  if(command->ggroups & (1<<GROUP_G_NONMODAL) && DECIMAL_INT(&command->gvals[GROUP_G_NONMODAL]) == 4)
    {
      ret = parser->out->dwell(parser->ctx, command->params[PARAM_P]);
      command->ggroups &= ~(1<<GROUP_G_NONMODAL);
    }

  /* 11. set active plane (G17, G18, G19). */
  if(command->ggroups & (1<<GROUP_G_PLANE))
    {
      int cmd = DECIMAL_INT(&command->gvals[GROUP_G_PLANE]);

      if(cmd == 17)
        {
        ret = parser->out->select_plane(parser->ctx, PLANE_XY);
        }
      else if(cmd == 18)
        {
        ret = parser->out->select_plane(parser->ctx, PLANE_XZ);
        }
      else if(cmd == 19)
        {
        ret = parser->out->select_plane(parser->ctx, PLANE_YZ);
        }

      parser->gmodes[GROUP_G_PLANE] = command->gvals[GROUP_G_PLANE];
      command->ggroups &= ~(1<<GROUP_G_PLANE);
    }

  /* 12. set length units (G20, G21). */
  if(command->ggroups & (1<<GROUP_G_UNITS))
    {
      int cmd = DECIMAL_INT(&command->gvals[GROUP_G_UNITS]);

      if(cmd == 20)
        {
          ret = parser->out->use_length_units(parser->ctx, UNITS_INCHES);
        }
      else if(cmd == 21)
        {
          ret = parser->out->use_length_units(parser->ctx, UNITS_MM);
        }

      parser->gmodes[GROUP_G_UNITS] = command->gvals[GROUP_G_UNITS];
      command->ggroups &= ~(1<<GROUP_G_UNITS);
    }

  /* 13. cutter radius compensation on or off (G40, G41, G42) */
  if(command->ggroups & (1<<GROUP_G_RADCOMP))
    {
      printf("Set radius comp: G%s\n", decimal_str(&command->gvals[GROUP_G_RADCOMP]));
      parser->gmodes[GROUP_G_RADCOMP] = command->gvals[GROUP_G_RADCOMP];
      command->ggroups &= ~(1<<GROUP_G_RADCOMP);
    }

  /* 14. cutter length compensation on or off (G43, G49) */
  if(command->ggroups & (1<<GROUP_G_LENGTHOFF))
    {
      printf("Set length comp: G%s\n", decimal_str(&command->gvals[GROUP_G_LENGTHOFF]));
      parser->gmodes[GROUP_G_LENGTHOFF] = command->gvals[GROUP_G_LENGTHOFF];
      command->ggroups &= ~(1<<GROUP_G_LENGTHOFF);
    }

  /* 15. coordinate system selection (G54, G55, G56, G57, G58, G59, G59.1, G59.2, G59.3). */
  if(command->ggroups & (1<<GROUP_G_COORDSYS))
    {
      printf("Set coordinate system: G%s\n", decimal_str(&command->gvals[GROUP_G_COORDSYS]));
      parser->gmodes[GROUP_G_COORDSYS] = command->gvals[GROUP_G_COORDSYS];
      command->ggroups &= ~(1<<GROUP_G_COORDSYS);
    }

  /* 16. set path control mode (G61, G61.1, G64) */
  if(command->ggroups & (1<<GROUP_G_PATHCTRL))
    {
      printf("Set path control: G%s\n", decimal_str(&command->gvals[GROUP_G_PATHCTRL]));
      parser->gmodes[GROUP_G_PATHCTRL] = command->gvals[GROUP_G_PATHCTRL];
      command->ggroups &= ~(1<<GROUP_G_PATHCTRL);
    }

  /* 17. set distance mode (G90, G91). */
  if(command->ggroups & (1<<GROUP_G_DISTMODE))
    {
      int cmd = DECIMAL_INT(&command->gvals[GROUP_G_DISTMODE]);
      printf("Set distance mode: %s\n", cmd==90?"absolute":"relative");
      parser->gmodes[GROUP_G_DISTMODE] = command->gvals[GROUP_G_DISTMODE];
      command->ggroups &= ~(1<<GROUP_G_DISTMODE);
    }

  /* 18. set retract mode (G98, G99). */
  if(command->ggroups & (1<<GROUP_G_CANNEDRET))
    {
      printf("Set retract mode: G%s\n", decimal_str(&command->gvals[GROUP_G_CANNEDRET]));
      parser->gmodes[GROUP_G_CANNEDRET] = command->gvals[GROUP_G_CANNEDRET];
      command->ggroups &= ~(1<<GROUP_G_CANNEDRET);
    }

  /* 19. [non modal] home (G28, G30) or change coordinate system data (G10)
   * or set axis offsets (G92, G92.1, G92.2, G94). */
  if(command->ggroups & (1<<GROUP_G_NONMODAL))
    {
      int cmdi = DECIMAL_INT(&command->gvals[GROUP_G_NONMODAL]);
      int cmdf = DECIMAL_FRAC(&command->gvals[GROUP_G_NONMODAL]);
      if(cmdi == 28)
        {
          printf("[nonmodal] Home\n");
        }
      else if(cmdi == 30)
        {
          printf("[nonmodal] Secondary Home\n");
        }
      else if(cmdi == 10)
        {
          printf("[nonmodal] Set Coord system origin\n");
        }
      else if(cmdi == 92)
        {
          int axis;
printf("XXX %d\n", cmdf);
          if(cmdf == 0)
            {
              printf("[nonmodal] Offset coordinate systems and set parameters\n");
              if(command->aflags == 0)
                {
                  return ERROR_AXIS_REQUIRED;
                }

              for(axis=0;axis<6;axis++)
                {
                  if(command->aflags & (1<<axis))
                    {
                    decimal_sub(&parser->offsets[axis], &command->axes[axis], &parser->axes[axis]);
                    command->aflags &= ~(1<<axis); //value is consumed by this command
                    }
                }
              printf("[TODO] Save offsets to parameters 5211 to 5216\n");
            }
          else if(cmdf == 1 || cmdf == 2)
            {
              for(axis = 0; axis < 6; axis++)
                {
                  DECIMAL_SET_INT(&parser->offsets[axis], 0);
                }
              if(cmdf==1)
                {
                  printf("[TODO] Set parameters 5211 to 5216 to zero\n");
                }
            }
          else if(cmdf == 3)
            {
              printf("[TODO] Load offsets from parameters 5211 to 5216\n");
            }
        }
      else if(cmdi == 53)
        {
          printf("[nonmodal] Motion in machine coordinate system\n");
        }
      command->ggroups &= ~(1<<GROUP_G_NONMODAL);
    }

  /* 20. perform motion (G0 to G3, G80 to G89), as modified (possibly) by G53. */

  /* Determine what params are required for motion commands */
  if(DECIMAL_INT(&parser->gmodes[GROUP_G_PLANE]) == 17) //XY plane
    {
      useful_axis  [0] = AXIS_X;  useful_axis  [1] = AXIS_Y; useful_axis[2] = AXIS_Z;
      useful_offset[0] = PARAM_I; useful_offset[1] = PARAM_J;
    }
  else if(DECIMAL_INT(&parser->gmodes[GROUP_G_PLANE]) == 18) //YZ plane
    {
      useful_axis  [0] = AXIS_Y;  useful_axis  [1] = AXIS_Z; useful_axis[2] = AXIS_X;
      useful_offset[0] = PARAM_J; useful_offset[1] = PARAM_K;
    }
  else if(DECIMAL_INT(&parser->gmodes[GROUP_G_PLANE]) == 19) //XZ plane
    {
      useful_axis  [0] = AXIS_Z;  useful_axis  [1] = AXIS_X; useful_axis[2] = AXIS_Y;
      useful_offset[0] = PARAM_K; useful_offset[1] = PARAM_I;
    }
  axis_mask   = (1<<useful_axis  [0]) | (1<<useful_axis  [1]);
  offset_mask = (1<<useful_offset[0]) | (1<<useful_offset[1]);

  /* First update motion mode */
  if(command->ggroups & (1<<GROUP_G_MOTION))
    {
      int cmd = DECIMAL_INT(&command->gvals[GROUP_G_MOTION]);

      //Error if all axes are omitted for G0 and G1
      if(cmd == 0 || cmd == 1)
        {
          if(command->aflags == 0)
            {
              return ERROR_AXIS_REQUIRED;
            }
        }
      else if(cmd == 2 || cmd == 3)
        {
          //R or IJ,JK,IK are needed
          //error if BOTH axis words for the plane are omitted
          if((command->aflags & axis_mask) == 0)
            {
              return ERROR_AXIS_REQUIRED;
            }
          if(command->pflags & FLAG_R)
            {
              arc_radius_mode = true;
            }
          else if(command->pflags & offset_mask)
            {
              arc_radius_mode = false;
            }
          else
            {
              return ERROR_AXIS_REQUIRED;
            }
        }
      else if(cmd == 80)
        {
          if(command->aflags)
            {
              return ERROR_SYNTAX;
            }
        }

      parser->gmodes[GROUP_G_MOTION] = command->gvals[GROUP_G_MOTION];
      command->ggroups &= ~(1<<GROUP_G_MOTION);
    }

  /* Then execute motion according to CURRENT motion mode */
  if(command->aflags != 0)
    {
      decimal_t center[2];
      decimal_t dest[6];
      int cmd = DECIMAL_INT(&parser->gmodes[GROUP_G_MOTION]);

      //Compute final coordinates for simple motions
      //Canned cycles interprets coordinates in a different way
      if(cmd == 0 || cmd == 1 || cmd == 2 || cmd == 3)
        {
          int axis;
          for(axis = 0; axis<6; axis++)
            {
              dest[axis] = parser->axes[axis];
              if(command->aflags & (1<<axis))
                {
                  if(DECIMAL_INT(&parser->gmodes[GROUP_G_DISTMODE]) == 90)
                    {
                      dest[axis] = command->axes[axis];
                    }
                  else
                    {
                      decimal_add(&dest[axis], &dest[axis], &command->axes[axis]);
                    }
                  //Apply offsets
                  decimal_sub(&dest[axis], &dest[axis], &parser->offsets[axis]);
                }
            }
        }

      //For circular cuts, compute the arc center
      if(cmd == 2 || cmd == 3)
        {

          if(arc_radius_mode)
            {
              printf("arc in radius mode\n");
              // Center mode
              // Find middle between current pos and dest pos
              decimal_add(&center[0], &dest[useful_axis[0]], &parser->axes[useful_axis[0]]);
              decimal_half(&center[0]);
              decimal_add(&center[1], &dest[useful_axis[1]], &parser->axes[useful_axis[1]]);
              decimal_half(&center[1]);
              // Compute center of arc (left or right of center)
              //TODO
            }
          else
            {
              printf("arc in center mode\n");
              //Center mode
              decimal_add(&center[0], &parser->axes[useful_axis[0]], &command->params[useful_offset[0]]);
              decimal_add(&center[1], &parser->axes[useful_axis[1]], &command->params[useful_offset[1]]);
            }

        }

      //update destination
      if(cmd == 0 || cmd == 1 || cmd == 2 || cmd == 3)
        {
          memcpy(parser->axes, dest, 6 * sizeof(decimal_t));
        }

      //Do the motion
      if(cmd == 0)
        {
          ret = parser->out->straight_traverse(parser->ctx, parser->axes);
        }
      else if(cmd == 1)
        {
          ret = parser->out->straight_feed(parser->ctx, parser->axes );
        }
      else if(cmd == 2 || cmd == 3)
        {
          ret = parser->out->arc_feed(parser->ctx, parser->axes[useful_axis[0]],
                                                   parser->axes[useful_axis[1]],
                                                   center[0], center[1],
                                                   parser->axes[useful_axis[2]],
                                                   (cmd==2),
                                                   parser->axes+AXIS_A);
        }
      else if(cmd == 73)
        {
          printf("Tormach fast peck drilling\n");
        }
      else if(cmd == 80)
        {
          printf("Cancel motion\n");
        }
      else if(cmd == 81)
        {
          printf("Drilling canned cycle\n");
        }
      else if(cmd == 82)
        {
          printf("Drilling with dwell canned cycle\n");
        }
      else if(cmd == 83)
        {
          printf("Peck drilling canned cycle\n");
        }
      else if(cmd == 84)
        {
          printf("Right hand tapping canned cycle\n");
        }
      else if(cmd == 85)
        {
          printf("Boring, no dwell, feed out canned cycle\n");
        }
      else if(cmd == 86)
        {
          printf("Boring, spindle stop, rapid out canned cycle\n");
        }
      else if(cmd == 87)
        {
          printf("Back boring canned cycle\n");
        }
      else if(cmd == 88)
        {
          printf("Boring, spindle stop, rapid out canned cycle\n");
        }
      else if(cmd == 89)
        {
          printf("Boring, dwell, feed out canned cycle\n");
        }
      else
        {
          return ERROR_SYNTAX;
        }
    } //some axes present on line

  /* 21. stop (M0, M1, M2, M30, M60). */
  if(command->mgroups & (1<<GROUP_M_STOPPING))
    {
      printf("Stopping: G%s\n", decimal_str(&command->mvals[GROUP_M_STOPPING]));
      parser->mmodes[GROUP_M_STOPPING] = command->mvals[GROUP_M_STOPPING];
      command->mgroups &= ~(1<<GROUP_M_STOPPING);
    }

  /* Now display any GCODE that was not managed (should not happen) */
  for(i=0;i<GROUP_G_COUNT;i++)
    {
      if(command->ggroups & (1<<i))
        {
          printf("***UNMANAGED G%s\n", decimal_str(&command->gvals[i]));
        }
    }

  /* Now display any MCODE that was not managed (should not happen) */
  for(i=0;i<GROUP_M_COUNT;i++)
    {
      if(command->mgroups & (1<<i))
        {
          printf("***UNMANAGED M%s\n", decimal_str(&command->gvals[i]));
        }
    }

  return 0;
}

