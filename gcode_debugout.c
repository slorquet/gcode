/****************************************************************************
 * gcode/gcode_debugout.c
 *
 *   Copyright (C) 2018 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <stdint.h>
#include <stdio.h>

#include "decimal.h"
#include "gcode_output.h"

static int debugout_init                            (void* ctx);
static int debugout_end                             (void* ctx);
static int debugout_select_plane                    (void* ctx, int plane);
static int debugout_set_origin_offsets              (void* ctx, decimal_t axes[6]);
static int debugout_use_length_units                (void* ctx, int units);
static int debugout_set_traverse_rate               (void* ctx, decimal_t rate);
static int debugout_straight_traverse               (void* ctx, decimal_t axes[6]);
static int debugout_set_feed_rate                   (void* ctx, decimal_t rate);
static int debugout_set_feed_reference              (void* ctx, int reference);
static int debugout_set_motion_control_mode         (void* ctx, int mode);
static int debugout_start_speed_feed_synch          (void* ctx);
static int debugout_stop_speed_feed_synch           (void* ctx);
static int debugout_arc_feed                        (void* ctx, decimal_t end1, decimal_t end2, decimal_t center1, decimal_t center2, decimal_t endpoint, bool clockwise, decimal_t rots[3]);
static int debugout_dwell                           (void* ctx, decimal_t seconds); 
static int debugout_stop                            (void* ctx);
static int debugout_straight_feed                   (void* ctx, decimal_t axes[6]);
static int debugout_straight_probe                  (void* ctx, decimal_t axes[6]);
static int debugout_orient_spindle                  (void* ctx, decimal_t orientation, int direction);
static int debugout_set_spindle_speed               (void* ctx, decimal_t speed);
static int debugout_spindle_retract                 (void* ctx);
static int debugout_spindle_retract_traverse        (void* ctx);
static int debugout_start_spindle_clockwise         (void* ctx);
static int debugout_start_spindle_counterclockwise  (void* ctx);
static int debugout_stop_spindle_turning            (void* ctx);
static int debugout_use_no_spindle_force            (void* ctx);
static int debugout_use_no_spindle_torque           (void* ctx);
static int debugout_use_spindle_force               (void* ctx, decimal_t force);
static int debugout_use_spindle_torque              (void* ctx, decimal_t torque);
static int debugout_change_tool                     (void* ctx, int slot);
static int debugout_select_tool                     (void* ctx, int slot);
static int debugout_use_tool_length_offset          (void* ctx, decimal_t length);
static int debugout_clamp_axis                      (void* ctx, int axis);
static int debugout_comment                         (void* ctx, char *text);
static int debugout_disable_feed_override           (void* ctx);
static int debugout_disable_speed_override          (void* ctx);
static int debugout_enable_feed_override            (void* ctx);
static int debugout_enable_speed_override           (void* ctx);
static int debugout_flood_off                       (void* ctx);
static int debugout_flood_on                        (void* ctx);
static int debugout_message                         (void* ctx, char *text);
static int debugout_mist_off                        (void* ctx);
static int debugout_mist_on                         (void* ctx);
static int debugout_pallet_shuttle                  (void* ctx);
static int debugout_through_tool_off                (void* ctx);
static int debugout_through_tool_on                 (void* ctx);
static int debugout_turn_probe_off                  (void* ctx);
static int debugout_turn_probe_on                   (void* ctx);
static int debugout_unclamp_axis                    (void* ctx, int axis);  
static int debugout_optional_program_stop           (void* ctx);
static int debugout_program_end                     (void* ctx);
static int debugout_program_stop                    (void* ctx);
static int debugout_set_cutter_radius_compensation  (void* ctx, decimal_t radius);
static int debugout_start_cutter_radius_compensation(void* ctx, int side);
static int debugout_stop_cutter_radius_compensation (void* ctx);

struct gcode_output_s debug_gcode_output =
{
  debugout_init,
  debugout_end,
  debugout_select_plane,
  debugout_set_origin_offsets,
  debugout_use_length_units,
  debugout_set_traverse_rate,
  debugout_straight_traverse,
  debugout_set_feed_rate,
  debugout_set_feed_reference,
  debugout_set_motion_control_mode,
  debugout_start_speed_feed_synch,
  debugout_stop_speed_feed_synch,
  debugout_arc_feed,
  debugout_dwell,
  debugout_stop,
  debugout_straight_feed,
  debugout_straight_probe,
  debugout_orient_spindle,
  debugout_set_spindle_speed,
  debugout_spindle_retract,
  debugout_spindle_retract_traverse,
  debugout_start_spindle_clockwise,
  debugout_start_spindle_counterclockwise,
  debugout_stop_spindle_turning,
  debugout_use_no_spindle_force,
  debugout_use_no_spindle_torque,
  debugout_use_spindle_force,
  debugout_use_spindle_torque,
  debugout_change_tool,
  debugout_select_tool,
  debugout_use_tool_length_offset,
  debugout_clamp_axis,
  debugout_comment,
  debugout_disable_feed_override,
  debugout_disable_speed_override,
  debugout_enable_feed_override,
  debugout_enable_speed_override,
  debugout_flood_off,
  debugout_flood_on,
  debugout_message,
  debugout_mist_off,
  debugout_mist_on,
  debugout_pallet_shuttle,
  debugout_through_tool_off,
  debugout_through_tool_on,
  debugout_turn_probe_off,
  debugout_turn_probe_on,
  debugout_unclamp_axis, 
  debugout_optional_program_stop,
  debugout_program_end,
  debugout_program_stop,
  debugout_set_cutter_radius_compensation,
  debugout_start_cutter_radius_compensation,
  debugout_stop_cutter_radius_compensation,
};

static int debugout_init(void* ctx)
{
  printf(">>> Init machine\n");
  return 0;
}

static int debugout_end(void* ctx)
{
  printf(">>> End machine\n");
  return 0;
}

static int debugout_select_plane(void* ctx, int plane)
{
  printf(">>> Select plane %s\n", plane==0?"XY":plane==1?"YZ":"XZ");
  return 0;
}

static int debugout_set_origin_offsets(void* ctx, decimal_t axes[6])
{
  return 0;
}

static int debugout_use_length_units(void* ctx, int units)
{
  printf(">>> Use length units %d\n", units);
  return 0;
}

static int debugout_set_traverse_rate(void* ctx, decimal_t rate)
{
  printf(">>> Set traverse rate %s%u.%.*u\n", rate.neg?"-":"", rate.integer, DECIMAL_DIGITS, rate.frac);
  return 0;
}

static int debugout_straight_traverse(void* ctx, decimal_t axes[6])
{
  printf(">>> Straight traverse\n");
  return 0;
}

static int debugout_set_feed_rate(void* ctx, decimal_t rate)
{
  printf(">>> Set feed rate %s%u.%.*u\n", rate.neg?"-":"", rate.integer, DECIMAL_DIGITS, rate.frac);
  return 0;
}

static int debugout_set_feed_reference(void* ctx, int reference)
{
  printf(">>> Use feed reference %d\n", reference);
  return 0;
}

static int debugout_set_motion_control_mode(void* ctx, int mode)
{
  printf(">>> Use motion control mode %d\n", mode);
  return 0;
}

static int debugout_start_speed_feed_synch(void* ctx)
{
  printf(">>> Start speed feed sync\n");
  return 0;
}

static int debugout_stop_speed_feed_synch(void* ctx)
{
  printf(">>> Stop speed feed sync\n");
  return 0;
}

static int debugout_arc_feed(void* ctx, decimal_t end1, decimal_t end2, decimal_t center1, decimal_t center2, decimal_t endpoint, bool clockwise, decimal_t rots[3])
{
  printf(">>> Arc feed\n");
  return 0;
}

static int debugout_dwell(void* ctx, decimal_t seconds)
{
  printf(">>> Dwell for %s%u.%.*u seconds \n", seconds.neg?"-":"", seconds.integer, DECIMAL_DIGITS, seconds.frac);
  return 0;
}

static int debugout_stop(void* ctx)
{
  printf(">>> Stop motion\n");
  return 0;
}

static int debugout_straight_feed(void* ctx, decimal_t axes[6])
{
  printf(">>> Straight feed\n");
  return 0;
}

static int debugout_straight_probe(void* ctx, decimal_t axes[6])
{
  printf(">>> Straight probe\n");
  return 0;
}

static int debugout_orient_spindle(void* ctx, decimal_t orientation, int direction)
{
  return 0;
}

static int debugout_set_spindle_speed(void* ctx, decimal_t speed)
{
  printf(">>> Set spindle speed %s%u.%.*u\n", speed.neg?"-":"", speed.integer, DECIMAL_DIGITS, speed.frac);
  return 0;
}

static int debugout_spindle_retract(void* ctx)
{
  printf(">>> Spindle retract\n");
  return 0;
}

static int debugout_spindle_retract_traverse(void* ctx)
{
  printf(">>> Spindle retract traverse\n");
  return 0;
}

static int debugout_start_spindle_clockwise(void* ctx)
{
  printf(">>> Start spindle clockwise\n");
  return 0;
}

static int debugout_start_spindle_counterclockwise(void* ctx)
{
  printf(">>> Start spindle counterclockwise\n");
  return 0;
}

static int debugout_stop_spindle_turning(void* ctx)
{
  printf(">>> Stop spindle\n");
  return 0;
}

static int debugout_use_no_spindle_force(void* ctx)
{
  printf(">>> Don't use spindle force\n");
  return 0;
}

static int debugout_use_no_spindle_torque(void* ctx)
{
  printf(">>> Don't use spindle torque\n");
  return 0;
}

static int debugout_use_spindle_force(void* ctx, decimal_t force)
{
  printf(">>> Start using spindle force %s%u.%.*u\n", force.neg?"-":"", force.integer, DECIMAL_DIGITS, force.frac);
  return 0;
}

static int debugout_use_spindle_torque(void* ctx, decimal_t torque)
{
  printf(">>> Start using spindle torque %s%u.%.*u\n", torque.neg?"-":"", torque.integer, DECIMAL_DIGITS, torque.frac);
  return 0;
}

static int debugout_change_tool(void* ctx, int slot)
{
  printf(">>> Change tool %d\n", slot);
  return 0;
}

static int debugout_select_tool(void* ctx, int slot)
{
  printf(">>> Select tool %d\n", slot);
  return 0;
}

static int debugout_use_tool_length_offset(void* ctx, decimal_t length)
{
  printf(">>> Use tool length offset %s%u.%.*u\n", length.neg?"-":"", length.integer, DECIMAL_DIGITS, length.frac);
  return 0;
}

static int debugout_clamp_axis(void* ctx, int axis)
{
  printf(">>> Clamp axis %d\n", axis);
  return 0;
}

static int debugout_comment(void* ctx, char *text)
{
  printf(">>> Comment: %s\n", text);
  return 0;
}

static int debugout_disable_feed_override(void* ctx)
{
  printf(">>> Disable feed override\n");
  return 0;
}

static int debugout_disable_speed_override(void* ctx)
{
  printf(">>> Disable speed override\n");
  return 0;
}

static int debugout_enable_feed_override(void* ctx)
{
  printf(">>> Enable feed override\n");
  return 0;
}

static int debugout_enable_speed_override(void* ctx)
{
  printf(">>> Enable speed override\n");
  return 0;
}

static int debugout_flood_off(void* ctx)
{
  printf(">>> Flood off\n");
  return 0;
}

static int debugout_flood_on(void* ctx)
{
  printf(">>> Flood on\n");
  return 0;
}

static int debugout_message(void* ctx, char *text)
{
  printf(">>> Message: %s\n", text);
  return 0;
}

static int debugout_mist_off(void* ctx)
{
  printf(">>> Mist off\n");
  return 0;
}

static int debugout_mist_on(void* ctx)
{
  printf(">>> Mist on\n");
  return 0;
}

static int debugout_pallet_shuttle(void* ctx)
{
  printf(">>> Pallet shuttle\n");
  return 0;
}

static int debugout_through_tool_off(void* ctx)
{
  printf(">>> Through tool off\n");
  return 0;
}

static int debugout_through_tool_on(void* ctx)
{
  printf(">>> Through tool on\n");
  return 0;
}

static int debugout_turn_probe_off(void* ctx)
{
  printf(">>> Probe off\n");
  return 0;
}

static int debugout_turn_probe_on(void* ctx)
{
  printf(">>> Probe on\n");
  return 0;
}

static int debugout_unclamp_axis(void* ctx, int axis)
{
  printf(">>> Unclamp axis %d\n", axis);
  return 0;
}

static int debugout_optional_program_stop(void* ctx)
{
  printf(">>> Optional program stop\n");
  return 0;
}

static int debugout_program_end(void* ctx)
{
  printf(">>> Program end\n");
  return 0;
}

static int debugout_program_stop(void* ctx)
{
  printf(">>> Program stop\n");
  return 0;
}

static int debugout_set_cutter_radius_compensation(void* ctx, decimal_t radius)
{
  printf(">>> Cutter radius compensation %s%u.%.*u\n", radius.neg?"-":"", radius.integer, DECIMAL_DIGITS, radius.frac);
  return 0;
}

static int debugout_start_cutter_radius_compensation(void* ctx, int side)
{
  printf(">>> Start radius compensation on side %d\n", side);
  return 0;
}

static int debugout_stop_cutter_radius_compensation(void* ctx)
{
  printf(">>> Stop radius compensation\n");
  return 0;
}

