/****************************************************************************
 * gcode/gcode_parser.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "gcode_parser.h"
#include "gcode_interpreter.h"

/****************************************************************************
 * Definitions
 ****************************************************************************/

#ifndef CONFIG_GCODE_BUFFER_SIZE
#define CONFIG_GCODE_BUFFER_SIZE 256
#endif

/****************************************************************************
 * gcode_main
 ****************************************************************************/


char buffer[CONFIG_GCODE_BUFFER_SIZE];

extern struct gcode_output_s debug_gcode_output;

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int gcode_main(int argc, FAR char *argv[])
#endif
{
  FAR FILE *input = stdin;
  FAR char *ptr;
  int len;
  int ret;
  bool will_close = false;
  struct gcode_state_s   gstate;
  struct gcode_command_s gcommand;
  if(argc == 2)
    {
      input = fopen(argv[1], "r");
      if(input == NULL)
        {
          fprintf(stderr, "Cannot open input file '%s', errno=%d\n", argv[1], errno);
          return 1;
        }
      will_close = true;
    }

  gcode_interp_init(&gstate, &debug_gcode_output, NULL);
  gcode_interp_show(&gstate);          

  /* Parse each line */

  while(!feof(input))
    {
      ptr = fgets(buffer, sizeof(buffer), input);
      if(ptr==NULL)
        {
        break; //EOF
        }

      len = strlen(buffer);
      if(buffer[len-1] == '\n')
        {
          buffer[len-1] = 0;
          len -= 1;
        }

      if(buffer[len-1] == '\r')
        {
          buffer[len-1] = 0;
          len -= 1;
        }
      ret = gcode_parse(&gcommand, ptr);

      if(ret==0)
        {
          //gcode_command_show(&gcommand);
          ret = gcode_interpret(&gstate, &gcommand);
        }
      else
        {
          printf("PARSE ERROR %d\n",ret);
        }

      if(ret==0)
        {
          gcode_interp_show(&gstate);          
          printf("OK\n");
        }
      else
        {
          printf("RUN ERROR %d\n",ret);
        }

    }

  /* All done. */

  if(will_close)
    {
      fclose(input);
    }

  return OK;
}

