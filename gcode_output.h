#ifndef __GCODE_OUTPUT_H__
#define __GCODE_OUTPUT_H__

#include <stdbool.h>
#include "decimal.h"

#define PLANE_XY 0
#define PLANE_YZ 1
#define PLANE_XZ 2

#define UNITS_INCHES 0
#define UNITS_MM     1
#define UNITS_CM     2

#define REFERENCE_WORKPIECE 0
#define REFERENE_XYZ        1

#define MODE_EXACT_STOP 0
#define MODE_EXACT_PATH 1
#define MODE_CONTINUOUS 2

#define DIRECTION_STOPPED 0
#define DIRECTION_CLOCKWISE 1
#define DIRECTION_COUNTERCLOCKWISE 2

#define SIDE_COMP_LEFT 0
#define SIDE_COMP_RIGHT 1

struct gcode_output_s
{
  int (*init                            )(void *ctx);
  int (*end                             )(void *ctx);
  int (*select_plane                    )(void *ctx, int plane);
  int (*set_origin_offsets              )(void *ctx, decimal_t axes[6]);
  int (*use_length_units                )(void *ctx, int units);
  int (*set_traverse_rate               )(void *ctx, decimal_t rate);
  int (*straight_traverse               )(void *ctx, decimal_t axes[6]);
  int (*set_feed_rate                   )(void *ctx, decimal_t rate);
  int (*set_feed_reference              )(void *ctx, int reference);
  int (*set_motion_control_mode         )(void *ctx, int mode);
  int (*start_speed_feed_synch          )(void *ctx);
  int (*stop_speed_feed_synch           )(void *ctx);
  int (*arc_feed                        )(void *ctx, decimal_t end1, decimal_t end2, decimal_t center1, decimal_t center2, decimal_t endpoint, bool clockwise, decimal_t rots[3]);
  int (*dwell                           )(void *ctx, decimal_t seconds); 
  /* ellipse_feed not supported */
  int (*stop                            )(void *ctx);
  int (*straight_feed                   )(void *ctx, decimal_t axes[6]);
  int (*straight_probe                  )(void *ctx, decimal_t axes[6]);
  int (*orient_spindle                  )(void *ctx, decimal_t orientation, int direction);
  int (*set_spindle_speed               )(void *ctx, decimal_t speed);
  int (*spindle_retract                 )(void *ctx);
  int (*spindle_retract_traverse        )(void *ctx);
  int (*start_spindle_clockwise         )(void *ctx);
  int (*start_spindle_counterclockwise  )(void *ctx);
  int (*stop_spindle_turning            )(void *ctx);
  int (*use_no_spindle_force            )(void *ctx);
  int (*use_no_spindle_torque           )(void *ctx);
  int (*use_spindle_force               )(void *ctx, decimal_t force);
  int (*use_spindle_torque              )(void *ctx, decimal_t torque);
  int (*change_tool                     )(void *ctx, int slot);
  int (*select_tool                     )(void *ctx, int slot);
  int (*use_tool_length_offset          )(void *ctx, decimal_t length);
  int (*clamp_axis                      )(void *ctx, int axis);
  int (*comment                         )(void *ctx, char *text);
  int (*disable_feed_override           )(void *ctx);
  int (*disable_speed_override          )(void *ctx);
  int (*enable_feed_override            )(void *ctx);
  int (*enable_speed_override           )(void *ctx);
  int (*flood_off                       )(void *ctx);
  int (*flood_on                        )(void *ctx);
  int (*message                         )(void* ctx, char *text);
  int (*mist_off                        )(void *ctx);
  int (*mist_on                         )(void *ctx);
  int (*pallet_shuttle                  )(void *ctx);
  int (*through_tool_off                )(void *ctx);
  int (*through_tool_on                 )(void *ctx);
  int (*turn_probe_off                  )(void *ctx);
  int (*turn_probe_on                   )(void *ctx);
  int (*unclamp_axis                    )(void *ctx, int axis);  
  int (*optional_program_stop           )(void *ctx);
  int (*program_end                     )(void *ctx);
  int (*program_stop                    )(void *ctx);
  int (*set_cutter_radius_compensation  )(void *ctx, decimal_t radius);
  int (*start_cutter_radius_compensation)(void *ctx, int side);
  int (*stop_cutter_radius_compensation )(void *ctx);
};

#endif /* __GCODE_OUTPUT_H__ */

