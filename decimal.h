#ifndef __DECIMAL__H__
#define __DECIMAL__H__

#include <stdint.h>
#include <string.h>
#include <stdio.h>

#define DECIMAL_DIGITS 6

#if DECIMAL_DIGITS == 6
#define DECIMAL_FRAC_MAX 1000000UL
#else
#error unknown FRAC_MAX
#endif

struct decimal_s
{
  uint32_t neg    : 1;  /*1 for negative */
  uint32_t integer: 31; /* integer part */
  uint32_t frac;        /* fractional part, positive, in range 0..(10^DECIMAL_DIGITS)-1 */
};

typedef struct decimal_s decimal_t;

#define DECIMAL_INITIALIZER(sign, intval, frac) {(sign), (intval), (frac)}

#define DECIMAL_INIT_SINT(intval) { ((uint32_t)(intval)>>31), ((uint32_t)(intval) & 0x7FFFFFFF), 0 }

#define DECIMAL_INT(dec) ((dec)->integer)
#define DECIMAL_FRAC(dec) ((dec)->frac)

#define DECIMAL_SET_INT(dec, val) \
    do { \
    int v = (val); \
    bool n = (v<0); \
    (dec)->integer = n ? -v : v; \
    (dec)->neg = n; \
    (dec)->frac = 0; \
    } while(0)

static char buf[80];

static inline char *decimal_str(decimal_t *d)
{
  sprintf(buf, "%s%u.%.*u" , d->neg?"-":"", d->integer, DECIMAL_DIGITS, d->frac);
  return buf;
}

//Compare A and B
static inline int decimal_cmp(decimal_t *a, decimal_t *b)
{
  if(a->integer > b->integer)
    return 1;
  if(a->integer < b->integer)
    return -1;
  //a and b integers are equal
  if(a->frac < b->frac)
    return -1;
  if(a->frac > b->frac)
    return 1;

  return 0;
}

static inline void decimal_add(decimal_t *dest, decimal_t *a, decimal_t *b);

// Compute A-B
static inline void decimal_sub(decimal_t *dest, decimal_t *a, decimal_t *b)
{
  int32_t d,bor,diff;
  decimal_t other;
  if(a->neg && !b->neg)
    {
      //A is negative, B is positive
      //printf("A neg, B pos, turn sub into add\n");
      //Compute C+B where C = -A is positive
      memcpy(&other, a, sizeof(decimal_t));
      other.neg = 0;
      decimal_add(dest, b, &other);
      return;
    }
  if(!a->neg && b->neg)
    {
      //A is positive, B is negative
      //printf("A pos, B neg, turn sub into add\n");
      //Compute A+C where C = -B is positive
      memcpy(&other, b, sizeof(decimal_t));
      other.neg = 0;
      decimal_add(dest, a, &other);
      return;
    }

  //printf("sub two same sign\n");
  //Both numbers have the same sign
  bor = 0;
  d = a->frac - b->frac;
  //printf("frac A %u B%u d %d\n",a->frac, b->frac, d);
  if(d < 0)
    {
      d += DECIMAL_FRAC_MAX;
      bor = 1;
      //printf("borrow, new D %d\n", d);
    }
  diff = a->integer - b->integer;
  //printf("int A %u B %u d %d\n", a->integer, b->integer, diff);
  dest->frac = d;
  if(diff < 0)
    {
      //printf("diff is neg, change sign\n");
      dest->integer = -bor-diff;
      dest->neg = !dest->neg;
    }
  else
    {
      dest->integer = diff;
    }
  //make sure zero is positive
  if(dest->integer == 0 && dest->frac == 0)
    {
      dest->neg = 0;
    }
}

// Compute A+B
static inline void decimal_add(decimal_t *dest, decimal_t *a, decimal_t *b)
{
  uint32_t s,car;
  decimal_t other;
  if(a->neg && !b->neg)
    {
      //A is negative, B is positive
      //printf("A neg, B pos, turn add into sub\n");
      //Compute B-C, where C = -A is positive
      memcpy(&other, a, sizeof(decimal_t));
      other.neg = 0;
      decimal_sub(dest, b, &other);
      return;
    }
  else if(!a->neg && b->neg)
    {
      //A is positive, B is negative
      //printf("A pos, B neg, turn add into sub\n");
      //Compute A-C where C = -B is positive
      memcpy(&other, b, sizeof(decimal_t));
      other.neg = 0;
      decimal_sub(dest, a, &other);
      return;
    }

  //printf("add two same sign\n");
  //Both numbers have the same sign: we can just add their absolute values
  car = 0;
  s = a->frac + b->frac;
  if(s >= DECIMAL_FRAC_MAX)
    {
      s -= DECIMAL_FRAC_MAX;
      car = 1;
    }
  dest->integer = a->integer + b->integer + car;
  dest->frac = s;
  //make sure zero is positive
  if(dest->integer == 0 && dest->frac == 0)
    {
      dest->neg = 0;
    }
}

static inline void decimal_half(decimal_t *dec)
{
  dec->frac /= 2;
  dec->integer /= 2;
}

#endif // __DECIMAL__H__

