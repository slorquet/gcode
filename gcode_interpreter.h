#ifndef __GCODE_INTERPRETER_H__
#define __GCODE_INTERPRETER_H__

#include "decimal.h"
#include "gcode_parser.h"
#include "gcode_output.h"

struct gcode_state_s
{
  FAR struct gcode_output_s *out;
  FAR void *ctx;
  int t_value; //remember tool index after tool select
  decimal_t speed,feed;
  decimal_t axes[6];    // X Y Z A B C
  decimal_t offsets[6]; // X Y Z A B C
  decimal_t gmodes[GROUP_G_COUNT];
  decimal_t mmodes[GROUP_M_COUNT];
};

int gcode_interp_init(FAR struct gcode_state_s *parser,
                      FAR struct gcode_output_s *output, FAR void *ctx);
int gcode_interp_show(FAR struct gcode_state_s *parser);          
int gcode_interpret(FAR struct gcode_state_s *parser,
                    FAR struct gcode_command_s *command);

#endif // __GCODE_INTERPRETER_H__

